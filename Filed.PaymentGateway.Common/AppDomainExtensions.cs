﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace Filed.PaymentGateway.Common
{
    public static class AppDomainExtensions
    {
        private const string AllAssembliesKey = "AppDomainExtensions.AllCustomAssembliesKey";
        private const string AllTypesKey = "AppDomainExtensions.AllCustomTypesKey";
        private static readonly string[] FiledAssemblies = new[] { "Filed." };

        public static IEnumerable<Type> GetAllCustomTypes(this AppDomain domain)
        {
            if (domain == null)
            {
                throw new ArgumentNullException("domain");
            }

            // We cache types with the domain for performance
            var types = (IEnumerable<Type>)domain.GetData(AllTypesKey);
            if (types != null)
            {
                return types;
            }

            types = GetAllCustomAssemblies(domain)
                .SelectMany(asm => asm.GetTypes())
                .ToList();

            domain.SetData(AllTypesKey, types);
            return types;
        }

        public static Assembly[] GetAllCustomAssemblies(this AppDomain domain)
        {
            if (domain == null)
            {
                throw new ArgumentNullException("domain");
            }

            var assemblies = (Assembly[])domain.GetData(AllAssembliesKey);
            if (assemblies != null)
            {
                return assemblies;
            }

            var assemblyDirs = GetAssemblyDirectories(domain.SetupInformation);

            // Find all .dll and .exe files in directories used to load assemblies
            var extensions = new[] { "*.dll", "*.exe" };
            var assemblyFiles = assemblyDirs
                .SelectMany(dir => extensions.SelectMany(ext => Directory.GetFiles(dir, ext)))
                .Where(file => file.Contains(".vshost") == false);

            assemblies = assemblyFiles
                .Where(f => IsCustomAssembly(Path.GetFileName(f)))
                .Select(Assembly.LoadFrom)
                .Concat(domain.GetAssemblies().Where(asm => IsCustomAssembly(asm.GetName().Name)))
                .Distinct()
                .ToArray();

            domain.SetData(AllAssembliesKey, assemblies);
            return assemblies;
        }

        // Returns a list of all the directories used to search for assemblies
        public static IEnumerable<string> GetAssemblyDirectories(this AppDomainSetup setupInformation)
        {
            // Unless PrivateBinPathProbe is non-null, we always look in ApplicationBase
            // http://msdn.microsoft.com/en-us/library/system.appdomainsetup.privatebinpathprobe(v=vs.110).aspx
            if (setupInformation.PrivateBinPathProbe == null)
            {
                yield return setupInformation.ApplicationBase;
            }

            // We also search in a set of subdirectories listed in PrivateBinPath.
            // This is, e.g., the bin directory in ASP.NET, or test directories in nunit.
            if (string.IsNullOrEmpty(setupInformation.PrivateBinPath) == false)
            {
                foreach (var subdir in setupInformation.PrivateBinPath.Split(';'))
                {
                    yield return Path.Combine(setupInformation.ApplicationBase, subdir);
                }
            }
        }

        private static bool IsCustomAssembly(string name)
        {
            return FiledAssemblies.Any(assembly => name.StartsWith(assembly, StringComparison.OrdinalIgnoreCase));
        }
    }
}
