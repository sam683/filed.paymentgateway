﻿using System;
using System.Collections.Generic;
using Filed.PaymentGateway.Api.Domain.Validators;
using Filed.PaymentGateway.Api.Models.Commands;
using NUnit.Framework;

namespace Filed.PaymentGateway.Tests.Validators
{
    [TestFixture]
    public class RequestValidatorFactoryTests
    {
        RequestValidatorFactory _sut;

        [SetUp]
        public void Setup()
        {
            var validators = new List<IRequestValidator>();
            _sut = new RequestValidatorFactory(validators);
        }

        [Test]
        public void Fail_when_there_is_no_implemented_validator()
        {
            IApiRequest request = new TestApiRequestWithNoValidator();
            var ex = Assert.Throws<NotImplementedException>(() => _sut.Create(request));
            Assert.That(ex.Message == $"Could not find any validator for this request {request.GetType()}");
        }
    }
}
