﻿using System;
using Filed.PaymentGateway.Api.Domain.Validators;
using Filed.PaymentGateway.Api.Models.Commands;
using NUnit.Framework;

namespace Filed.PaymentGateway.Tests.Validators
{
    [TestFixture]
    public class PaymentsRequestValidatorTests
    {
        private PaymentsRequestValidator _sut;
        private IApiRequest _apiRequest;

        [SetUp]
        public void Setup()
        {
            _sut = new PaymentsRequestValidator();
        }

        [Test]
        public void Fail_validation_when_request_is_null()
        {
            _apiRequest = null;
            var validationResult = _sut.Validate(_apiRequest);

            Assert.IsFalse(validationResult);
        }

        [Test]
        public void Fail_validation_when_credit_card_number_is_invalid()
        {
            _apiRequest = GetACorrectRequest();
            ((PaymentRequest) _apiRequest).CreditCardNumber += "ABC";

            var validationResult = _sut.Validate(_apiRequest);

            Assert.IsFalse(validationResult);
        }

        [Test]
        public void Fail_validation_when_amount_is_negative()
        {
            _apiRequest = GetACorrectRequest();
            ((PaymentRequest) _apiRequest).Amount = - 400;

            var validationResult = _sut.Validate(_apiRequest);

            Assert.IsFalse(validationResult);
        }

        [Test]
        public void Fail_validation_when_the_card_is_expired()
        {
            _apiRequest = GetACorrectRequest();
            ((PaymentRequest) _apiRequest).ExpirationDate = DateTime.Now.AddDays(-1);

            var validationResult = _sut.Validate(_apiRequest);

            Assert.IsFalse(validationResult);
        }

        [Test]
        public void Fail_validation_when_card_holder_name_is_empty()
        {
            _apiRequest = GetACorrectRequest();
            ((PaymentRequest) _apiRequest).CardHolder = null;

            var validationResult = _sut.Validate(_apiRequest);

            Assert.IsFalse(validationResult);
        }

        [Test]
        public void Fail_validation_when_the_security_code_is_more_than_three_digits()
        {
            _apiRequest = GetACorrectRequest();
            ((PaymentRequest) _apiRequest).SecurityCode = "1361";

            var validationResult = _sut.Validate(_apiRequest);

            Assert.IsFalse(validationResult);
        }

        [Test]
        public void Pass_validation_when_the_request_is_correct()
        {
            _apiRequest = GetACorrectRequest();

            var validationResult = _sut.Validate(_apiRequest);

            Assert.IsTrue(validationResult);
        }

        private static PaymentRequest GetACorrectRequest()
        {
            return new PaymentRequest
            {
                CreditCardNumber = "5566887203953414",
                Amount = 100,
                ExpirationDate = DateTime.Now.AddDays(365),
                CardHolder = "Mrs Testington",
                SecurityCode = "683"
            };
        }
    }
}
