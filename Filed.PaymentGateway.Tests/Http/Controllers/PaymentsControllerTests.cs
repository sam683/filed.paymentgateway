﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Filed.PaymentGateway.Api.Domain.Handlers.interfaces;
using Filed.PaymentGateway.Api.Host.Controllers;
using Filed.PaymentGateway.Api.Models.Commands;
using Moq;
using NUnit.Framework;

namespace Filed.PaymentGateway.Tests.Http.Controllers
{
    [TestFixture]
    public class PaymentsControllerTests
    {
        private Mock<IPaymentHandler> _paymentHandlerMock;

        [SetUp]
        public void Setup()
        {
            _paymentHandlerMock = new Mock<IPaymentHandler>();
        }

        [Test]
        public void Return_bad_request_if_request_is_null()
        {
            var controller = new PaymentsController(_paymentHandlerMock.Object)
            {
                Request = new HttpRequestMessage(),
                Configuration = new HttpConfiguration(),
            };

            var response = controller.ProcessPayment(null)
                .GetAwaiter()
                .GetResult();
            
            Assert.IsTrue(response.StatusCode == HttpStatusCode.BadRequest);
        }

        [Test]
        public void Return_200_if_request_is_successfully_processed()
        {
            _paymentHandlerMock
                .Setup(p=> p.ProcessPayment(It.IsAny<PaymentRequest>()))
                .Returns(Task.FromResult(false))
                .Verifiable();

            var controller = new PaymentsController(_paymentHandlerMock.Object)
            {
                Request = new HttpRequestMessage(),
                Configuration = new HttpConfiguration(),
            };

            var request = new PaymentRequest
            {
                CreditCardNumber =  "5566887203953414",
                Amount = 100,
                ExpirationDate = DateTime.Now.AddDays(365),
                CardHolder = "Mr Testington",
                SecurityCode = "683"
            };

            var response = controller.ProcessPayment(request)
                .GetAwaiter()
                .GetResult();
            
            Assert.IsTrue(response.StatusCode == HttpStatusCode.OK);
        }
    }
}
