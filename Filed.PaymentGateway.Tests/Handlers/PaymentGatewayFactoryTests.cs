﻿using Filed.PaymentGateway.Api.Domain.Handlers;
using Filed.PaymentGateway.Api.Domain.PaymentGateways;
using NUnit.Framework;
using NUnit.Framework.Internal;

namespace Filed.PaymentGateway.Tests.Handlers
{
    [TestFixture()]
    public class PaymentGatewayFactoryTests
    {
        private PaymentGatewayFactory _sut;

        [SetUp]
        public void Setup()
        {
            _sut = new PaymentGatewayFactory();
        }
        
        [Test]
        public void When_amount_is_less_than_20_then_CheapPaymentGateway_should_handle_the_request()
        {
            var paymentGateway = _sut.Create(18);
            Assert.AreEqual(paymentGateway.GetType(), typeof(CheapPaymentGateway));
        }

        [Test]
        public void When_amount_is_more_than_500_then_PremiumPaymentGateway_should_handle_the_request()
        {
            var paymentGateway = _sut.Create(501);
            Assert.AreEqual(paymentGateway.GetType(), typeof(PremiumPaymentGateway));
        }

        [Test]
        public void When_amount_is_between_21_and_500_then_PremiumPaymentGateway_should_handle_the_request()
        {
            var paymentGateway = _sut.Create(300);
            Assert.AreEqual(paymentGateway.GetType(), typeof(HybridPaymentGateway));
        }
    }
}
