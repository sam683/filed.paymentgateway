﻿using System;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using Filed.PaymentGateway.Api.Domain.Handlers;
using Filed.PaymentGateway.Api.Domain.PaymentGateways.interfaces;
using Filed.PaymentGateway.Api.Domain.Validators;
using Filed.PaymentGateway.Api.Models.Commands;
using Moq;
using NUnit.Framework;
using PaymentRequest = Filed.PaymentGateway.Domain.Entity.PaymentRequest;
using ApiPaymentRequest = Filed.PaymentGateway.Api.Models.Commands.PaymentRequest;

namespace Filed.PaymentGateway.Tests.Handlers
{
    [TestFixture]
    public class PaymentHandlerTests
    {
        private Mock<IRequestValidatorFactory> _irequestValidatorFactory;
        private Mock<IRequestValidator> _irequestValidator;
        private Mock<IPaymentGatewayFactory> _ipaymentGatewayFactoryMock;
        private Mock<IPaymentGateway> _ipaymentGatewayMock;
        private PaymentHandler _sut;

        [SetUp]
        public void Setup()
        {

            _irequestValidatorFactory = new Mock<IRequestValidatorFactory>();
            _irequestValidator = new Mock<IRequestValidator>();
            _ipaymentGatewayFactoryMock = new Mock<IPaymentGatewayFactory>();
            _ipaymentGatewayMock = new Mock<IPaymentGateway>();
            
            _irequestValidator.Setup(i => i.Validate(It.IsAny<IApiRequest>()))
                .Returns(true);

            _irequestValidatorFactory.Setup(i => i.Create(It.IsAny<IApiRequest>()))
                .Returns(_irequestValidator.Object);

            _sut = new PaymentHandler(_irequestValidatorFactory.Object, _ipaymentGatewayFactoryMock.Object);
        }

        

        [Test]
        public void Fail_when_payment_did_not_happen()
        {
            var request = new ApiPaymentRequest
            {
                CreditCardNumber =  "5566887203953414",
                Amount = 100,
                ExpirationDate = DateTime.Now.AddDays(365),
                CardHolder = "Mr Testington",
                SecurityCode = "683"
            };

            _ipaymentGatewayMock.Setup(i => i.MakePayment(It.IsAny<PaymentRequest>()))
                .Returns(Task.FromResult(false));

            _ipaymentGatewayFactoryMock.Setup(i => i.Create(It.IsAny<decimal>()))
                .Returns(_ipaymentGatewayMock.Object);
            
            
            var ex = Assert.ThrowsAsync<HttpResponseException>(async () => await _sut.ProcessPayment(request));
            Assert.That(ex.Response.StatusCode, Is.EqualTo(HttpStatusCode.InternalServerError));
        }
    }
}
