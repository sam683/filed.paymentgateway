﻿using System;

namespace Filed.PaymentGateway.Domain.Entity
{
    public class PaymentRequest
    {
        public Guid RequestId { get; set; }
        public string CreditCardNumber { get; set; }
        public string CardHolder { get; set; }
        public DateTime ExpirationDate { get; set; }
        public string SecurityCode { get; set; }
        public decimal Amount { get; set; }
    }
}
