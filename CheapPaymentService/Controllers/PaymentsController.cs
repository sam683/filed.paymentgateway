﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using Filed.PaymentGateway.Api.Models.Commands;


namespace CheapPaymentService.Controllers
{
    public class PaymentsController : ApiController
    {
        [HttpPost]
        [Route("payments")]
        public HttpResponseMessage ProcessPayment(PaymentRequest request)
        {
            return Request.CreateResponse(HttpStatusCode.OK);
        }
    }
}
