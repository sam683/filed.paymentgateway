## Filed Payment Gateway
Asynchronous Rest API with retry mechanism.


### Description:
This project will expose a payment api and integrates with three different Restful 3rd parties: 

- PremiumPaymentGateway

- ExpensivePaymentGatewa

- CheapPaymentGateway



### Solution entry point
Filed.PaymentGateway.Api.Host

### API specification

Address: 
```

[POST] http://localhost:6081/payments

```

Input: input data is a Payment Request object that holds few parameters as below:
```
{
	"CreditCardNumber": 5566887203953414,
	"CardHolder": "ahmad mousavi",
	"ExpirationDate": "01/01/2020",
	"SecurityCode" : 123,
	"Amount" : 501
}

```

Output: it is an HttpResponseMessage with three possible values:
```
200 - HttpStatusCode.OK
400 - HttpStatusCode.BadRequest
500 - HttpStatusCode.InternalServerError
```

### Integration with payment gateways
There are three webapis in the solution to act as integration layer.
these three gateways are 
```
http://localhost:8000/payments
http://localhost:8001/payments
http://localhost:8002/payments
```


### how to run
```
	.net framework 4.6.2 is required.
	nuget restore

```

### unit test
this project uses nunit. you can find all the unit tests in here
```
Filed.PaymentGateway.Tests.Unit
```

