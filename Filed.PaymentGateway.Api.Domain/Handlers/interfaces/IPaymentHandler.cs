﻿using System.ComponentModel.Composition;
using System.Threading.Tasks;
using Filed.PaymentGateway.Api.Models.Commands;

namespace Filed.PaymentGateway.Api.Domain.Handlers.interfaces
{
    [InheritedExport]
    public interface IPaymentHandler
    {
        Task ProcessPayment(PaymentRequest request);
    }
}
