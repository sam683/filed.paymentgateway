﻿using System;
using System.Net;
using System.Threading.Tasks;
using Filed.PaymentGateway.Api.Domain.Constants;
using Filed.PaymentGateway.Api.Domain.Handlers.interfaces;
using Filed.PaymentGateway.Api.Domain.PaymentGateways.interfaces;
using Filed.PaymentGateway.Api.Domain.Validators;
using Filed.PaymentGateway.Api.Models.Commands;
using DomainEntity = Filed.PaymentGateway.Domain.Entity;
namespace Filed.PaymentGateway.Api.Domain.Handlers
{
    public class PaymentHandler : ApiHandlerBase, IPaymentHandler
    {
        private readonly IPaymentGatewayFactory _paymentGatewayFactory;

        public PaymentHandler(IRequestValidatorFactory validatorFactory,
            IPaymentGatewayFactory paymentGatewayFactory) :
            base(validatorFactory)
        {
            _paymentGatewayFactory = paymentGatewayFactory;
        }
        public async Task ProcessPayment(PaymentRequest request)
        {
            ValidateRequest(request);

            var paymentGateway =
                _paymentGatewayFactory.Create(request.Amount);

            var moneyPaied = await paymentGateway.MakePayment(new DomainEntity.PaymentRequest
            {
                RequestId = Guid.NewGuid(),
                Amount = request.Amount,
                CardHolder = request.CardHolder,
                CreditCardNumber = request.CreditCardNumber,
                ExpirationDate = request.ExpirationDate,
                SecurityCode = request.SecurityCode
            });

            if (!moneyPaied)
            {
                RespondWithError(ValidationErrors.GeneralServerError,
                    HttpStatusCode.InternalServerError);
            }
        }
    }
}
