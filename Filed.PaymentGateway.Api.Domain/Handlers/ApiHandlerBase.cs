﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using Filed.PaymentGateway.Api.Domain.Constants;
using Filed.PaymentGateway.Api.Domain.Validators;
using Filed.PaymentGateway.Api.Models.Commands;

namespace Filed.PaymentGateway.Api.Domain.Handlers
{
    public abstract class ApiHandlerBase
    {
        private readonly IRequestValidatorFactory _iValidatorFactory;

        protected ApiHandlerBase(IRequestValidatorFactory iValidatorFactory)
        {
            _iValidatorFactory = iValidatorFactory;
        }

        protected static void RespondWithError(string errorMessage,
            HttpStatusCode statusToReturn = HttpStatusCode.BadRequest)
        {
            var exception = new HttpResponseException(

                new HttpResponseMessage
                {
                    StatusCode = statusToReturn,
                    Content = new StringContent(errorMessage)
                });

            throw exception;
        }

        protected void ValidateRequest(IApiRequest request)
        {
            var validator = _iValidatorFactory.Create(request);

            var validationResult = validator.Validate(request);
            if (!validationResult)
            {
                RespondWithError(ValidationErrors.InvalidPaymentRequest);
            }
        }
    }
}
