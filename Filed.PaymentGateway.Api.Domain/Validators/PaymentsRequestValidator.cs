﻿using System;
using Filed.PaymentGateway.Api.Models.Commands;

namespace Filed.PaymentGateway.Api.Domain.Validators
{
    public class PaymentsRequestValidator: IRequestValidator
    {
        public bool Validate(IApiRequest apiRequest)
        {
            var request = apiRequest as PaymentRequest;

            if (request == null)
            {
                return false;
            }

            if (!CreditCardNumberIsValid(request.CreditCardNumber)
                || string.IsNullOrEmpty(request.CardHolder)
                || CardIsExpired(request.ExpirationDate)
                || !SecurityCodeIsValid(request.SecurityCode)
                || !AmountIsValid(request.Amount))
            {
                return false;
            }

            return true;
        }
        public Type GetTargetRequestToValidate()
        {
            return typeof(PaymentRequest);
        }

        //From https://www.codeproject.com/Articles/36377/Validating-Credit-Card-Numbers 
        private static bool CreditCardNumberIsValid(string creditCardNumber)
        {
            int i, checkSum = 0;

            for (i = creditCardNumber.Length - 1; i >= 0; i -= 2)
                checkSum += (creditCardNumber[i] - '0');

            for (i = creditCardNumber.Length - 2; i >= 0; i -= 2)
            {
                int val = ((creditCardNumber[i] - '0') * 2);
                while (val > 0)
                {
                    checkSum += (val % 10);
                    val /= 10;
                }
            }
            return ((checkSum % 10) == 0);
        }
        private static bool CardIsExpired(DateTime exirydatDateTime)
        {
            return exirydatDateTime < DateTime.Now;
        }
        private static bool SecurityCodeIsValid(string securityCode)
        {
            if (string.IsNullOrEmpty(securityCode))
                return true;

            if (securityCode.Length != 3)
                return false;

            if (!int.TryParse(securityCode, out int cv2))
            {
                return false;
            }

            return true;
        }
        private static bool AmountIsValid(decimal amount)
        {
            return amount > 0;
        }
    }
}
