﻿using System;
using System.ComponentModel.Composition;
using Filed.PaymentGateway.Api.Models.Commands;

namespace Filed.PaymentGateway.Api.Domain.Validators
{
    [InheritedExport]
    public interface IRequestValidator
    {
        bool Validate(IApiRequest request);
        Type GetTargetRequestToValidate();
    }
}
