﻿using System;
using System.Collections.Generic;
using System.Linq;
using Filed.PaymentGateway.Api.Models.Commands;

namespace Filed.PaymentGateway.Api.Domain.Validators
{
    public class RequestValidatorFactory : IRequestValidatorFactory
    {
        private readonly IEnumerable<IRequestValidator> _requestValidators;

        public RequestValidatorFactory(IEnumerable<IRequestValidator> requestValidators)
        {
            _requestValidators = requestValidators;
        }

        public IRequestValidator Create(IApiRequest apiRequest)
        {
            var validator = _requestValidators.FirstOrDefault(x => x.GetTargetRequestToValidate() == apiRequest.GetType());
            if (validator != null)
                return validator;

            throw new NotImplementedException($"Could not find any validator for this request {apiRequest.GetType()}");
        }
    }
}
