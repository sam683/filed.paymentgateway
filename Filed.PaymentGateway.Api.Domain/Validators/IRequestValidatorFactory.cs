﻿using System.ComponentModel.Composition;
using Filed.PaymentGateway.Api.Models.Commands;
namespace Filed.PaymentGateway.Api.Domain.Validators
{
    [InheritedExport]
    public interface IRequestValidatorFactory
    {
        IRequestValidator Create(IApiRequest apiRequest);
    }
}
