﻿namespace Filed.PaymentGateway.Api.Domain.Constants
{
    //TODO: I could be specific about the error. I made it general error message for the sake of demonstration
    public static class ValidationErrors
    {
        public const string InvalidPaymentRequest =
            "The request is invalid. please check the parameters";
        public const string GeneralServerError =
            "Something went wrong. please contact the administrators";
    }
}