﻿using System.Threading.Tasks;
using Filed.PaymentGateway.Domain.Entity;

namespace Filed.PaymentGateway.Api.Domain.PaymentGateways.interfaces
{
    public interface IPaymentGateway
    {
        Task<bool> MakePayment(PaymentRequest paymentRequest);
    }
}
