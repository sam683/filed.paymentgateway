﻿namespace Filed.PaymentGateway.Api.Domain.PaymentGateways.interfaces
{
    public interface IPaymentGatewayFactory
    {
        IPaymentGateway Create(decimal paymentAmount);
    }
}
