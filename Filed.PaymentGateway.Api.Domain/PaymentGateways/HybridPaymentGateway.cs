﻿using System.Threading.Tasks;
using Filed.PaymentGateway.Api.Domain.PaymentGateways.interfaces;
using Filed.PaymentGateway.Domain.Entity;

namespace Filed.PaymentGateway.Api.Domain.PaymentGateways
{
    public class HybridPaymentGateway  : PaymentGatewayBase, IPaymentGateway
    {
        public async Task<bool> MakePayment(PaymentRequest paymentRequest)
        {
            var paid = await base.MakePayment(
                paymentRequest: paymentRequest,
                endpointconfigurationKey: "ExpensivePaymentServiceEndpoint",
                maxRetry: 0);

            if (!paid)
            {
                paid = await base.MakePayment(
                    paymentRequest: paymentRequest,
                    endpointconfigurationKey: "CheapPaymentServiceEndpoint",
                    maxRetry: 0);
            }

            return paid;
        }
    }
}
