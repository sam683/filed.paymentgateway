﻿using System.Threading.Tasks;
using Filed.PaymentGateway.Api.Domain.PaymentGateways.interfaces;
using Filed.PaymentGateway.Domain.Entity;

namespace Filed.PaymentGateway.Api.Domain.PaymentGateways
{
    public class PremiumPaymentGateway : PaymentGatewayBase, IPaymentGateway
    {
        public async Task<bool> MakePayment(PaymentRequest paymentRequest)
        {
            return await base.MakePayment(
                paymentRequest: paymentRequest,
                endpointconfigurationKey: "PremiumPaymentServiceEndpoint",
                maxRetry: 3);
        }
    }
}