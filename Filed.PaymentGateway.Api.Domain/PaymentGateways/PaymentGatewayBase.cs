﻿using System.Configuration;
using System.Net;
using System.Threading.Tasks;
using Filed.PaymentGateway.Domain.Entity;
using Polly;
using RestSharp;

namespace Filed.PaymentGateway.Api.Domain.PaymentGateways
{
    public abstract class PaymentGatewayBase
    {
        protected async Task<bool> MakePayment(PaymentRequest paymentRequest, 
            string endpointconfigurationKey, int maxRetry)
        {
            var policy = await Policy
                .HandleResult<IRestResponse>(r => r.StatusCode != HttpStatusCode.OK)
                .RetryAsync(maxRetry)
                .ExecuteAndCaptureAsync(() =>
                {
                    var endpointAddress = ConfigurationManager.AppSettings[endpointconfigurationKey];
                    IRestClient client = new RestClient(endpointAddress);
                    IRestRequest request = new RestRequest(Method.POST);
                    var response = client.ExecutePostTaskAsync(request);
                    return response;
                });

            return policy.Outcome == OutcomeType.Successful;
        }
    }
}
