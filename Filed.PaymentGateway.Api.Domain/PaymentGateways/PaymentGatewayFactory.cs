﻿using Filed.PaymentGateway.Api.Domain.PaymentGateways.interfaces;

namespace Filed.PaymentGateway.Api.Domain.PaymentGateways
{
    public class PaymentGatewayFactory: IPaymentGatewayFactory
    {
        public IPaymentGateway Create(decimal paymentAmount)
        {
            if (paymentAmount < 20)
                return new CheapPaymentGateway();

            if (paymentAmount > 500)
                return new PremiumPaymentGateway();

            return new HybridPaymentGateway();
        }
    }
}
