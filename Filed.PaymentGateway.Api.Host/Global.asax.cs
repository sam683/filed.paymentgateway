﻿using System;
using System.Web.Http;
using Filed.PaymentGateway.Api.Host.Configurations;

namespace Filed.PaymentGateway.Api.Host
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start(object sender, EventArgs e)
        {
            GlobalConfiguration.Configure(ConfigureWebApi);
        }

        private void ConfigureWebApi(HttpConfiguration config)
        {
            AutofacConfig.ConfigureAutofacInjection(config);
            RouteConfig.ConfigureRouting(config);
        }
    }
}
