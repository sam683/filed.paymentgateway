﻿using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Threading.Tasks;
using System.Web.Http;
using Filed.PaymentGateway.Api.Domain.Handlers.interfaces;
using Filed.PaymentGateway.Api.Models.Commands;
using log4net;

namespace Filed.PaymentGateway.Api.Host.Controllers
{
    public class PaymentsController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private readonly IPaymentHandler _paymentHandler;

        public PaymentsController(IPaymentHandler paymentHandler)
        {
            _paymentHandler = paymentHandler;
        }

        [HttpPost]
        [Route("payments")]
        public async Task<HttpResponseMessage> ProcessPayment(PaymentRequest request)
        {
            if (request == null)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }

            Log.InfoFormat("Handling PaymentRequest");

            await _paymentHandler.ProcessPayment(request);

            return Request.CreateResponse(HttpStatusCode.OK);
        }
    }
}
