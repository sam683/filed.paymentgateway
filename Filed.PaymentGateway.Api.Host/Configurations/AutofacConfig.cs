﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Autofac;
using Autofac.Core;
using Autofac.Integration.WebApi;
using Filed.PaymentGateway.Common;
using System.ComponentModel.Composition;
using Filed.PaymentGateway.Api.Host.Controllers;

namespace Filed.PaymentGateway.Api.Host.Configurations
{
    public static class AutofacConfig
    {
        public static void ConfigureAutofacInjection(HttpConfiguration config)
        {
            var resolver = new AutofacWebApiDependencyResolver(GetContainer());
            config.DependencyResolver = resolver;
        }

        private static IContainer GetContainer()
        {
            var builder = new ContainerBuilder();

            RegisterApiControllers(builder);

            var customTypes = AppDomain.CurrentDomain.GetAllCustomTypes().ToList();

            RegisterCustomModules(customTypes, builder);

            RegisterCustomTypes(customTypes, builder);

            return builder.Build();
        }

        private static void RegisterCustomModules(IEnumerable<Type> types, ContainerBuilder builder)
        {
            foreach (var type in types.Where(IsModule))
            {
                builder.RegisterModule((IModule)Activator.CreateInstance(type));
            }
        }

        private static void RegisterApiControllers(ContainerBuilder builder)
        {
            builder.RegisterAssemblyTypes(
                    typeof(PaymentsController).Assembly)
                .Where(t => !t.IsAbstract && typeof(ApiController).IsAssignableFrom(t))
                .InstancePerRequest();
        }

        private static void RegisterCustomTypes(IEnumerable<Type> types, ContainerBuilder builder)
        {
            foreach (var type in types.Where(ShouldRegisterType))
            {
                builder.RegisterType(type)
                    .AsSelf()
                    .AsImplementedInterfaces()
                    .PropertiesAutowired()
                    .InstancePerRequest();
            }
        }

        private static bool IsModule(Type t)
        {
            return typeof(IModule).IsAssignableFrom(t) &&
                   t.IsAbstract == false;
        }

        private static bool ShouldRegisterType(Type type)
        {
            if (type.IsAbstract)
            {
                return false;
            }

            if (type.IsDefined(typeof(ExportAttribute), false))
            {
                return true;
            }

            if (type.BaseType != null && type.BaseType.IsDefined(typeof(InheritedExportAttribute), true))
            {
                return true;
            }

            return type.GetInterfaces().Any(i => i.IsDefined(typeof(InheritedExportAttribute), true));
        }
    }
}