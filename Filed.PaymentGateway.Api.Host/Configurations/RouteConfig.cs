﻿using System.Web.Http;

namespace Filed.PaymentGateway.Api.Host.Configurations
{
    public class RouteConfig
    {
        public static void ConfigureRouting(HttpConfiguration config)
        {
            config.MapHttpAttributeRoutes();
        }
    }
}