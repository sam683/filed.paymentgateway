﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Filed.PaymentGateway.Api.Models.Commands;


namespace RemiumPaymentService.Controllers
{
    public class PaymentsController : ApiController
    {
        [HttpPost]
        [Route("payments")]
        public HttpResponseMessage ProcessPayment(PaymentRequest request)
        {
            //Intentionally made this api flaky to simulate retry mechanism
            var number = new Random().Next(2);
            var httpStatus = (number == 0 ? HttpStatusCode.OK
                : HttpStatusCode.InternalServerError);
            return Request.CreateResponse(httpStatus);
        }
    }
}
