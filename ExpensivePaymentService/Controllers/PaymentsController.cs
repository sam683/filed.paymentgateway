﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using Filed.PaymentGateway.Api.Models.Commands;

namespace ExpensivePaymentService.Controllers
{
    public class PaymentsController : ApiController
    {
        [HttpPost]
        [Route("payments")]
        public HttpResponseMessage ProcessPayment(PaymentRequest request)
        {
            //Intentionally made this api faulty to simulate a hybrid payment gateway.
            return Request.CreateResponse(HttpStatusCode.InternalServerError);
        }
    }
}
